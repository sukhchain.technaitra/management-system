<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function __construct()
{
    $this->middleware('admin')->except(['index', 'updateTaskStatus']);
}

    public function index()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $tasks = Task::query();

            if ($user->role_id != 1) {
                $tasks->where('assign_to_user', $user->id);
            }

            $tasks = $tasks->get();

            $users = User::where('role_id', '==', 2)->get();

            return view('tasks.index', compact('tasks', 'users'));
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $users =  User::where('role_id', '!=', 1)->get();
        return view('tasks.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'due_date' => 'required|date',
            'priority' => 'required|in:1,2,3',
        ]);

        $task = new Task();
        $task->title = $request->title;
        $task->description = $request->description;
        $task->due_date = $request->due_date;
        $task->priority = $request->priority;
        $task->status = $request->status ?? 'pending';
        $task->assign_to_user = $request->assign_to_user;
        $task->save();

        return redirect()->route('tasks.index')->with('success', 'Task created successfully.');
    }
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Task $task)
    {
        $users =  User::where('role_id', '!=', 1)->get();
        return view('tasks.edit', compact('task', 'users'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Task $task)
    {
        $request->validate([
            'title' => 'required',
            'due_date' => 'required|date',
            'priority' => 'required|in:1,2,3',
        ]);

        $task->title = $request->title;
        $task->description = $request->description;
        $task->due_date = $request->due_date;
        $task->priority = $request->priority;
        $task->status = $request->status ?? 'pending';
        $task->assign_to_user = $request->assign_to_user;
        $task->save();

        return redirect()->route('tasks.index')->with('success', 'Task updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Task $task)
    {
        $task->delete();
        return redirect()->route('tasks.index')->with('success', 'Task deleted successfully.');
    }
    public function updateTaskStatus(Request $request, $id)
    {
        $request->validate([
            'status' => 'required|in:pending,in_progress,completed'
        ]);

        $task = Task::findOrFail($id);
        $task->status = $request->input('status');
        $task->save();

        return redirect()->route('tasks.index')->with('success', 'Task updated successfully.');
    }
}
