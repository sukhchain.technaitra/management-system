<?php

namespace App\Http\Middleware;


use Closure;

class AdminMiddleware
{
    public function handle($request, Closure $next)
    {
        if (!auth()->check() || !auth()->user()->isAdmin()) {
            return redirect('/home')->with('error', 'You are not authorized to access this page.');
        }

        return $next($request);
    }
}
